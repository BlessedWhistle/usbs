## Welcome to the US BS service

### Please Note!!

Right now this script is in the testing phase. So when you run it, it will not send any emails. They will just be printed out in the terminal. We are still looking for good email verbaige and subject line before it will be ready to send. If you are good at writing and would like to help we welcome your help :)

### What you will need

- Python 3 installed
  (_if you need help installing python3 on Windows 10 you can wach this vid: https://www.youtube.com/watch?v=V_ACbv4329E, python3 is usually installed by default on Macs and most Linux distros_)
- A Gmail account,
  (_create a burner one if you want because you will have to enter your gmail password in the script for it to work_)
- Turn on less secure apps for your gmail account, you can do so by going here: https://myaccount.google.com/lesssecureapps

### How to Run

- First update the send_emails.py script with your gmail address and password

```python
EMAIL_ADDRESS = "YOUR-EMAIL@GMAIL.COM"  # replace with your gmail account in the quotes
EMAIL_PASSWORD = "PASSWORD"  # replace with your gmail passowrd in the quotes
```

- Then from the same directory that the send_emails.py file is in run this command in bash/powershell/cmd

```bash
python3 send_emails.py
```

- Sit back and wait for it to complete :)

### How long will it take to send the emails.

It will probably take a good while for it to send all the emails, maybe half hour to an hour depending on your connection speed.

### The Email List

The email list comes from the addresses posted here: https://thedonald.win/p/11RNjzE88Y/dear-ga-pedes-2min-contact-your-/c/

I took that info and compiled it down to a csv file called address.csv

Here is the legend for the info of each record in the csv:

- 0 = Email Address
- 1 = First Name
- 2 = Last Name
- 3 = Party Inital D or R -> can make it spell out Democrat or Republican in the email template if needed
- 4 = Congress or Senate
