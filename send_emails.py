import os
import smtplib
import time
import random
import csv
from email.message import EmailMessage

'''
NOTE:
Make sure to turn on Less Secure Apps for your google account or it will not work
You can do so here: https://myaccount.google.com/lesssecureapps
'''
EMAIL_ADDRESS = "YOUR-EMAIL@GMAIL.COM"  # replace with your gmail account in the quotes
EMAIL_PASSWORD = "PASSWORD"  # replace with your gmail passowrd in the quotes


def random_slow():
    seconds = random.randint(0, 5)
    return seconds


def party_word(party):
    if party == 'D':
        return 'Democrat'
    else:
        return 'Republican'


def start_mailing():

    # opening the CSV file
    with open('address.csv', mode='r')as file:

        '''
        CSV Legend
        0 = Email Address
        1 = First Name
        2 = Last Name
        3 = Party Initial
        4 = Congress or Senate
        '''

        # reading the CSV file
        csvFile = csv.reader(file)

        # start sending emails 1 at a time
        for lines in csvFile:

            # Email Subject
            EMAIL_SUBJECT = 'The Subject Line'

            # Email Body Template
            EMAIL_BODY = f'''
            Hello {lines[1]}, you are in {lines[4]} and a member of the {party_word(lines[3])} party.
            i am just writing to let you know that you are an ass.
            -- Suck it
            '''

            # Create the Email Message
            msg = EmailMessage()
            msg.set_content(EMAIL_BODY)

            msg['Subject'] = EMAIL_SUBJECT
            msg['From'] = EMAIL_ADDRESS
            msg['To'] = lines[0]

            # Send the Email Message
            try:
                server = smtplib.SMTP('smtp.gmail.com', 587)
                server.ehlo()
                server.starttls()
                server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
                # server.sendmail(sent_from, to, email_text)
                server.send_message(msg)
                server.quit()
                print(f'sent mail to {lines[0]}')
            except:
                print('Something went wrong... - Make sure you have less secure apps turned on here: https://myaccount.google.com/lesssecureapps')

            # sleep for a random interval
            time.sleep(random_slow())


def test_mailing():
    # opening the CSV file
    with open('address.csv', mode='r')as file:

        '''
        CSV Legend
        0 = Email Address
        1 = First Name
        2 = Last Name
        3 = Party Initial
        4 = Congress or Senate
        '''

        # reading the CSV file
        csvFile = csv.reader(file)

        # start sending emails 1 at a time
        for lines in csvFile:

            # Email Subject
            EMAIL_SUBJECT = 'The Subject Line'

            # Email Body Template
            EMAIL_BODY = f'''
            Hello {lines[1]}, you are in {lines[4]} and a member of the {party_word(lines[3])} party.
            i am just writing to let you know that you are an ass.
            -- Suck it
            '''
            TEMPLATE = f'''
            FROM: {EMAIL_ADDRESS}
            TO: {lines[0]}

            SUBJECT: {EMAIL_SUBJECT}
            BODY: {EMAIL_BODY}
            '''

            print(TEMPLATE)
            print('waiting...')

            # sleep for a random interval
            time.sleep(random_slow())


def main():
    test_mailing()
    # start_mailing()


if __name__ == "__main__":
    main()
